package com.example.mealmate.domain.usecase.auth

import com.example.mealmate.domain.model.User
import com.example.mealmate.domain.repository.AuthRepository
import javax.inject.Inject

class Signup @Inject constructor(private val repository: AuthRepository) {

    suspend operator fun invoke(user: User) = repository.signUp(user)

}