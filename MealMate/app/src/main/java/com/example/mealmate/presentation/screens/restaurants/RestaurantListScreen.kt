package com.example.mealmate.presentation.screens.restaurants

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.material.Icon
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import com.example.mealmate.R
import com.example.mealmate.domain.model.Restaurant
import com.example.mealmate.presentation.screens.restaurants.components.RestaurantItem
import kotlinx.coroutines.launch


@Composable
fun RestaurantScreen(viewModel: RestaurantViewModel = hiltViewModel(), navController: NavHostController) {
    Scaffold(
        /*bottomBar = {
            BottomAppBar {
                IconButton(onClick = {  }) {
                    Icon(Icons.Filled.Place, contentDescription = "Localized description")
                }
                /*IconButton(onClick = { /* doSomething() */ }) {
                    Icon(Icons.Filled.Favorite, contentDescription = "Localized description")
                }*/
            }
        }*/
    ) { contentPadding ->
        Box(
            Modifier
                .padding(contentPadding)
                .fillMaxSize()) {
        Filters(Modifier.align(Alignment.Center), viewModel, navController)
        }
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun Filters(modifier: Modifier, viewModel: RestaurantViewModel, navController: NavHostController) {
    Column(modifier = modifier) {

        val restaurants by viewModel.restaurants.observeAsState(emptyList())

        val sheetState = rememberModalBottomSheetState(
            initialValue = ModalBottomSheetValue.Hidden,
            confirmStateChange = { it != ModalBottomSheetValue.HalfExpanded }
        )
        val coroutineScope = rememberCoroutineScope()

        BackHandler(sheetState.isVisible) {
            coroutineScope.launch { sheetState.hide() }
        }

        ModalBottomSheetLayout(
            sheetState = sheetState,
            sheetContent = { BottomSheet(viewModel.restaurantNoMod,viewModel) },
            modifier = Modifier.fillMaxSize()
        ) {
            Column(
                modifier = Modifier
                    .fillMaxSize(),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Row(modifier = Modifier.fillMaxWidth()) {
                    TopAppBar(
                        title = {
                            Text(text = "Restaurants", modifier = Modifier.padding(horizontal = 25.dp), fontSize = 22.sp, fontWeight = FontWeight.Bold)
                        },
                        backgroundColor = MaterialTheme.colors.background,
                        navigationIcon = {
                            IconButton(onClick = {/* Do Something*/ }) {
                                Icon(Icons.Filled.ArrowBack, contentDescription = "Flecha de regreso")
                            }
                        }, actions = {
                            /*IconButton(onClick = {
                                coroutineScope.launch {
                                    if (sheetState.isVisible) sheetState.hide()
                                    else sheetState.show()
                                }
                            }
                            ) {
                                Icon(painterResource(id = R.drawable.filter_list_black_24dp), contentDescription = "Filtros")
                            }*/
                        })
                }
                AllRestaurants(restList = restaurants, viewModel)
            }
        }
    }
}



@Composable
fun AllRestaurants(restList: List<Restaurant>, viewModel: RestaurantViewModel) {
    Box(
        Modifier
            .fillMaxSize()
            .padding(16.dp)
            .clip(RoundedCornerShape(12.dp))
    ) {
        LazyColumn(
            modifier = Modifier
                .background(Color.Red)
        ) {
            items(restList) { restaurant ->
                RestaurantItem(restaurant, viewModel)
            }
        }
    }
}




@Composable
fun BottomSheet(restList: List<Restaurant>, viewModel: RestaurantViewModel){
    Column(
        modifier = Modifier
            .padding(top = 15.dp)
            .height(320.dp)
    ) {

        Text(text = "Filters", modifier = Modifier.padding(horizontal = 25.dp), fontSize = 22.sp, fontWeight = FontWeight.Bold)
        Spacer(modifier = Modifier.height(20.dp))

        val COLOR1 = 0x80FFFFFF
        val COLOR2 = 0x80FFF64740

        val colorCont1 = 0xFF000000
        val colorCont2 = 0xFFFFFFFF

        val noSelect= false
        val select = true


        var selectVeg by remember {
            mutableStateOf(noSelect)
        }

        var colorVeg by remember {
            mutableStateOf(COLOR1)
        }

        var colContVeg by remember {
            mutableStateOf(colorCont1)
        }

        Button(onClick = {
            colorVeg = when (colorVeg){
                COLOR1 -> COLOR2
                else ->  COLOR1
            }

            selectVeg = when (selectVeg){
                noSelect -> select
                else -> noSelect
            }

            colContVeg = when (colContVeg){
                colorCont1 -> colorCont2
                else -> colorCont1
            }

        }, modifier = Modifier
            .fillMaxWidth()
            .height(40.dp)
            .padding(horizontal = 25.dp),
            colors = ButtonDefaults.buttonColors(
                backgroundColor = Color(colorVeg),
                contentColor = Color(colContVeg)
            ),
        ) {
            Icon(
                painterResource(id = R.drawable.local_florist_black_24dp),
                contentDescription = "Flor"
            )
            Spacer(modifier = Modifier.width(8.dp))
            Text(text = "Vegetarian", fontWeight = FontWeight.Bold)
        }
        Spacer(modifier = Modifier.height(8.dp))

        var colorBurg by remember {
            mutableStateOf(COLOR1)
        }

        var selectHambu by remember {
            mutableStateOf(noSelect)
        }

        var colContHambu by remember {
            mutableStateOf(colorCont1)
        }

        Button(onClick = {
            colorBurg = when (colorBurg){
                COLOR1 -> COLOR2
                else ->  COLOR1
            }

            selectHambu = when (selectHambu){
                noSelect -> select
                else -> noSelect
            }

            colContHambu = when (colContHambu){
                colorCont1 -> colorCont2
                else -> colorCont1
            }
        }, modifier = Modifier
            .fillMaxWidth()
            .height(40.dp)
            .padding(horizontal = 25.dp),
            colors = ButtonDefaults.buttonColors(
                backgroundColor = Color(colorBurg),
                contentColor = Color(colContHambu)
            )) {
            Icon(
                painterResource(id = R.drawable.lunch_dining_black_24dp),
                contentDescription = "Hamburger"
            )
            Spacer(modifier = Modifier.width(8.dp))
            Text(text = "Burguers", fontWeight = FontWeight.Bold)
        }
        Spacer(modifier = Modifier.height(8.dp))

        var colorPiz by remember {
            mutableStateOf(COLOR1)
        }

        var selectPiz by remember {
            mutableStateOf(noSelect)
        }

        var colContPiz by remember {
            mutableStateOf(colorCont1)
        }

        Button(onClick = {
            colorPiz = when (colorPiz){
                COLOR1 -> COLOR2
                else ->  COLOR1
            }

            selectPiz = when (selectPiz){
                noSelect -> select
                else -> noSelect
            }

            colContPiz = when (colContPiz){
                colorCont1 -> colorCont2
                else -> colorCont1
            }
        }, modifier = Modifier
            .fillMaxWidth()
            .height(40.dp)
            .padding(horizontal = 25.dp),
            colors = ButtonDefaults.buttonColors(
                backgroundColor = Color(colorPiz),
                contentColor = Color(colContPiz)
            )) {
            Icon(
                painterResource(id = R.drawable.local_pizza_black_24dp),
                contentDescription = "Pizza"
            )
            Spacer(modifier = Modifier.width(8.dp))
            Text(text = "Pizza", fontWeight = FontWeight.Bold)
        }
        Spacer(modifier = Modifier.height(8.dp))

        var colorItal by remember {
            mutableStateOf(COLOR1)
        }

        var selectIta by remember {
            mutableStateOf(noSelect)
        }

        var colContIta by remember {
            mutableStateOf(colorCont1)
        }

        Button(onClick = {
            colorItal = when (colorItal){
                COLOR1 -> COLOR2
                else ->  COLOR1
            }

            selectIta = when (selectIta){
                noSelect -> select
                else -> noSelect
            }

            colContIta = when (colContIta){
                colorCont1 -> colorCont2
                else -> colorCont1
            }
        }, modifier = Modifier
            .fillMaxWidth()
            .height(40.dp)
            .padding(horizontal = 25.dp)
            ,
            colors = ButtonDefaults.buttonColors(
                backgroundColor = Color(colorItal),
                contentColor = Color(colContIta)
            )) {
            Icon(
                painterResource(id = R.drawable.coffee_black_24dp),
                contentDescription = "Italiana"
            )
            Spacer(modifier = Modifier.width(8.dp))
            Text(text = "Italian", fontWeight = FontWeight.Bold)
        }
        Spacer(modifier = Modifier.height(20.dp))
        /*Button(onClick = {
            viewModel.modifiRestanurantFilter(selectVeg,selectHambu,selectPiz,selectIta,restList)
        }, modifier = Modifier
            .fillMaxWidth()
            .height(40.dp)
            .padding(horizontal = 25.dp)
            ,
            colors = ButtonDefaults.buttonColors(backgroundColor = Color(0x80FF2198f6))) {
            Text(text = "Apply Filters", fontWeight = FontWeight.Bold, color = Color(
                0xFFFEFFFF
            ), fontSize = 16.sp
            )
        }*/
    }
}