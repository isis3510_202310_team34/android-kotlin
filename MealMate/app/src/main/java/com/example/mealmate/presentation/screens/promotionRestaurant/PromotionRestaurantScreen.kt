package com.example.mealmate.presentation.screens.promotionRestaurant

import android.annotation.SuppressLint
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import com.example.mealmate.R

@Preview
@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun PromotionRestaurantScreen() {

    Scaffold(
        topBar = { TopBar() },
        content = { Content() }
    )

}

@Composable
fun TopBar(){
    Row(modifier = Modifier.fillMaxWidth()) {
        TopAppBar(
            title = {
                Text(text = "Promotion", modifier = Modifier.padding(horizontal = 25.dp), fontSize = 22.sp, fontWeight = FontWeight.Bold)
            },
            backgroundColor = MaterialTheme.colors.background,
            navigationIcon = {
                IconButton(onClick = {/* Do Something*/ }) {
                    Icon(Icons.Filled.ArrowBack, contentDescription = "Flecha de regreso")
                }
            })
    }
}


@Composable
fun Content() {
    LazyColumn(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        item { Spacer(modifier = Modifier.height(70.dp)) }
        item { ImageApp() }
        item { Spacer(modifier = Modifier.height(50.dp)) }
        item { ColumnContent() }

    }
}

@Composable
fun ColumnContent(){
    Column(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        ButtomCreatePromotion()
        Spacer(modifier = Modifier.height(50.dp))
        ButtomPromotionList()
        Spacer(modifier = Modifier.height(50.dp))
    }
}

@Composable
fun ImageApp(){
    AsyncImage(
        modifier = Modifier
            .clip(shape = RoundedCornerShape(7.dp))
            .size(size = 300.dp),
        model = R.drawable.ic_taco,
        contentDescription = "Imágen App",
        contentScale = ContentScale.Crop
    )
}

@Composable
fun ButtomCreatePromotion() {

    val background = 0x80FFF64740
    val colorCont = 0xFFFFFFFF

    Button(
        onClick = {},
        modifier = Modifier
            .fillMaxWidth()
            .height(50.dp)
            .padding(horizontal = 25.dp)
            .clip(RoundedCornerShape(100.dp)),
        colors = ButtonDefaults.buttonColors(
            backgroundColor = Color(background),
            contentColor = Color(colorCont)
        )
    ) {
        Icon(
            painterResource(id = R.drawable.sharp_design_create_fore),
            contentDescription = "Create Promotion"
        )
        Spacer(modifier = Modifier.width(8.dp))
        Text(text = "Create Promotion", fontWeight = FontWeight.Bold)
    }
}

@Composable
fun ButtomPromotionList() {

    val background = 0x80FFF64740
    val colorCont = 0xFFFFFFFF
    Button(
        onClick = {},
        modifier = Modifier
            .fillMaxWidth()
            .height(50.dp)
            .padding(horizontal = 25.dp)
            .clip(RoundedCornerShape(100.dp)),
        colors = ButtonDefaults.buttonColors(
            backgroundColor = Color(background),
            contentColor = Color(colorCont)
        )
    ) {
        Icon(
            painterResource(id = R.drawable.format_list_bullet_m),
            contentDescription = "Promotions List"
        )
        Spacer(modifier = Modifier.width(8.dp))
        Text(text = "Promotions List", fontWeight = FontWeight.Bold)
    }
}
