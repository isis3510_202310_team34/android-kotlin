package com.example.mealmate.data.network

import com.example.mealmate.domain.model.User
import kotlinx.coroutines.tasks.await
import javax.inject.Inject

class UserService @Inject constructor(private val firebase: FirebaseClient) {

    companion object {
        const val USER_COLLECTION = "users"
    }

    suspend fun createUserTable(userSignIn: User) = runCatching {

        val user = hashMapOf(
            "email" to userSignIn.email,
            "name" to userSignIn.name,
            "phone" to userSignIn.phone,
            "role" to userSignIn.role
        )

        firebase.db
            .collection(USER_COLLECTION)
            .add(user).await()

    }.isSuccess
}