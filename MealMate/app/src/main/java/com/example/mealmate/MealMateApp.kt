package com.example.mealmate

import android.app.Application
import dagger.hilt.android.HiltAndroidApp
//import timber.log.Timber
//import timber.log.Timber.DebugTree

@HiltAndroidApp
class MealMateApp : Application() {}