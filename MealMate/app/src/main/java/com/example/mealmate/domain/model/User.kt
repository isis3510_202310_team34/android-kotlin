package com.example.mealmate.domain.model

import com.google.gson.Gson

data class User(
    var id: String = "",
    var name: String = "",
    var email: String = "",
    var password: String = "",
    var phone: String = "",
    var role: String = ""
) {
    fun toJson(): String = Gson().toJson(
        User(
        id,
        name,
        email,
        password,
        phone,
        role
    )
    )

    companion object {
        fun fromJson(data: String): User = Gson().fromJson(data, User::class.java)
    }

}