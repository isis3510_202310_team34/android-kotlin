package com.example.mealmate.presentation.screens.restaurants

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mealmate.domain.usecase.auth.AuthUseCases
import com.example.mealmate.domain.model.Restaurant
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject


@HiltViewModel
class RestaurantViewModel @Inject constructor(
    private val authUseCases: AuthUseCases,
    private val restaurantRepository: RestaurantRepository
    ): ViewModel() {
    private val currentUser = authUseCases.getCurrentUser()

    //private val db = Firebase.firestore
    //private val restaurantsRef = db.collection("restaurants")
    //private val usersRef = db.collection("users")

    //private val _restaurants = MutableLiveData<List<Restaurant>>()
    //val restaurants: LiveData<List<Restaurant>> = _restaurants
    var restaurantNoMod: List<Restaurant> = listOf()
    val restaurants: MutableLiveData<List<Restaurant>> = restaurantRepository.getRestaurants()

    fun toggleFavorite(restaurant: Restaurant) {
        restaurant.isFavorite = !restaurant.isFavorite
        restaurantRepository.updateFavorites(restaurant.id, restaurant.isFavorite)
    }

    /*init {
        restaurantsRef.get().addOnSuccessListener {result ->
            val restaurants = result.documents.map { document ->
                val restaurant = document.toObject(Restaurant::class.java)!!
                restaurant.copy(id = document.id)
            }
            currentUser?.let { user ->
                usersRef.document(user.uid).get().addOnSuccessListener { userDoc ->
                    val favoriteRestaurants = userDoc["favorite_restaurants"] as? List<*>
                    favoriteRestaurants?.let { favorites ->
                        restaurants.forEach(){ rest ->
                            rest.isFavorite = favorites.contains(rest.id)
                        }
                        // update the list of restaurants in the ViewModel
                        _restaurants.value = restaurants
                        restaurantNoMod = restaurants
                    }
                }
            } ?: run {
                // update the list of restaurants in the ViewModel
                _restaurants.value = restaurants
                restaurantNoMod = restaurants
            }
        }.addOnFailureListener {exception ->
            Log.e(TAG, "Error getting restaurants", exception)
        }
    }*/

    //Without Repository
    /*private fun updateFavorites(restaurantId: String, isFavorite: Boolean) {
        val uid = currentUser?.uid
        uid?.let {
            val userRef = db.collection("users").document(it)
            if (isFavorite) {
                userRef.update("favorite_restaurants", FieldValue.arrayUnion(restaurantId))
                    .addOnSuccessListener {
                        Log.d(TAG, "Added $restaurantId to favorites")
                    }
                    .addOnFailureListener { e ->
                        Log.w(TAG, "Error adding $restaurantId to favorites", e)
                    }
            } else {
                userRef.update("favorite_restaurants", FieldValue.arrayRemove(restaurantId))
                    .addOnSuccessListener {
                        Log.d(TAG, "Removed $restaurantId from favorites")
                    }
                    .addOnFailureListener { e ->
                        Log.w(TAG, "Error removing $restaurantId from favorites", e)
                    }
            }
        }
    }*/

    //FILTROS
    /*fun modifiRestanurantFilter(selectVeg:Boolean,selectHambu:Boolean, selectPiz:Boolean, selectIta:Boolean, restList: List<Restaurant>){
        var fil = listOf<String>()
        var listRestModi = listOf<Restaurant>()

        if (selectVeg){
            val vege = "Vegetarian"
            fil = fil+vege
        }
        if (selectHambu){
            val bur = "Burgers"
            fil = fil+bur
        }
        if (selectPiz){
            val piz = "Pizza"
            fil = fil+piz
        }
        if (selectIta){
            val ita = "Italian"
            fil = fil+ita
        }

        for(i in restList.indices){
            if (filtro(restList[i].tags,fil)){
                listRestModi = listRestModi+restList[i]
            }
        }

        println("RestList: " + restList)
        println("Veg: " + selectVeg)
        println("Piz: " + selectPiz)
        println("LISSTA DE MODIFICADOS: " + listRestModi)
        restaurants.value = listRestModi

        if (!selectVeg && !selectHambu && !selectPiz && !selectIta){
            restaurants.value = restList
        }

    }

    private fun filtro(lista:List<String>, tags: List<String>):Boolean{

        var exist = false

        for(i in tags.indices){
            if(lista.contains(tags[i])){
                exist=true
            }
        }

        return exist
    }*/
}