package com.example.mealmate.domain.model

import com.google.firebase.firestore.GeoPoint

data class Restaurant(
    var id: String = "",
    val name: String = "",
    val image: String = "",
    val tags: List<String> = listOf(""),
    val location: GeoPoint = GeoPoint(0.0, 0.0),
    var isFavorite: Boolean = false,
)

