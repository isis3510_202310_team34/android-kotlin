package com.example.mealmate.di

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.example.mealmate.core.Constants.USERS
import com.example.mealmate.data.repository.AuthRepositoryImpl
import com.example.mealmate.data.repository.UsersRepositoryImpl
import com.example.mealmate.domain.usecase.users.Create
import com.example.mealmate.domain.repository.AuthRepository
import com.example.mealmate.domain.repository.UsersRepository
import com.example.mealmate.domain.usecase.auth.*
import com.example.mealmate.domain.usecase.users.GetUserById
import com.example.mealmate.domain.usecase.users.Update
import com.example.mealmate.domain.usecase.users.UsersUseCases
import com.example.mealmate.domain.usecase.auth.*
import com.example.mealmate.domain.usecase.users.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Named

@InstallIn(SingletonComponent::class)
@Module
object AppModule {

    @Provides
    fun provideFirebaseFirestore(): FirebaseFirestore = Firebase.firestore

    @Provides
    @Named(USERS)
    fun provideUsersRef(db: FirebaseFirestore): CollectionReference = db.collection(USERS)

    @Provides
    fun provideFirebaseAuth(): FirebaseAuth = FirebaseAuth.getInstance()

    @Provides
    fun provideAuthRepository(impl: AuthRepositoryImpl): AuthRepository = impl

    @Provides
    fun provideUsersRepository(impl: UsersRepositoryImpl): UsersRepository = impl

    @Provides
    fun provideAuthUseCases(repository: AuthRepository) = AuthUseCases(
        getCurrentUser = GetCurrentUser(repository),
        login = Login(repository),
        logout = Logout(repository),
        signup = Signup(repository)
    )

    @Provides
    fun provideUsersUseCases(repository: UsersRepository) = UsersUseCases(
        create = Create(repository),
        getUserById = GetUserById(repository),
        update = Update(repository)
    )

}