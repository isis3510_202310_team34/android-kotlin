package com.example.mealmate.domain

import com.example.mealmate.data.network.AuthenticationService
import com.example.mealmate.data.response.LoginResult
import javax.inject.Inject

class LoginUseCase @Inject constructor(private val authenticationService: AuthenticationService) {

    suspend operator fun invoke(email: String, password: String): LoginResult =
        authenticationService.login(email, password)
}