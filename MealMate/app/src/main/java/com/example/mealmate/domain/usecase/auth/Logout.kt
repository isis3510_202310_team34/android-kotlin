package com.example.mealmate.domain.usecase.auth

import com.example.mealmate.domain.repository.AuthRepository
import javax.inject.Inject

class Logout @Inject constructor(private val repository: AuthRepository) {

    operator fun invoke() = repository.logout()

}