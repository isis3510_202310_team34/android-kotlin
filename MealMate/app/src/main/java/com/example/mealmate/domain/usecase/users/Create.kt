package com.example.mealmate.domain.usecase.users

import com.example.mealmate.domain.model.User
import com.example.mealmate.domain.repository.UsersRepository
import javax.inject.Inject

class Create @Inject constructor(private val repository: UsersRepository) {

    suspend operator fun invoke(user: User) = repository.create(user)

}