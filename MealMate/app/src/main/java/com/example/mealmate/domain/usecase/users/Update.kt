package com.example.mealmate.domain.usecase.users

import com.example.mealmate.domain.model.User
import com.example.mealmate.domain.repository.UsersRepository
import javax.inject.Inject

class Update @Inject constructor(private val repository: UsersRepository) {

    suspend operator fun invoke(user: User) = repository.update(user)

}