package com.example.mealmate.domain.repository

import com.example.mealmate.domain.model.Response
import com.example.mealmate.domain.model.User
import kotlinx.coroutines.flow.Flow

interface UsersRepository {

    suspend fun create(user: User): Response<Boolean>
    suspend fun update(user: User): Response<Boolean>
    //suspend fun saveImage(file: File): Response<String>
    fun getUserById(id: String): Flow<User>

}