package com.example.mealmate.presentation.screens.login.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Email
import androidx.compose.material.icons.filled.Lock
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import com.example.mealmate.R
import com.example.mealmate.presentation.common_components.DefaultButton
import com.example.mealmate.presentation.common_components.DefaultTextField
import com.example.mealmate.presentation.screens.login.LoginViewModel


@Composable
fun LoginContent(navController: NavHostController, viewModel: LoginViewModel = hiltViewModel()) {

    val state = viewModel.state


    Box(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp)
    ) {
        Column(
            modifier = Modifier.align(Alignment.Center).padding(40.dp)
        ) {


            Image(
                modifier = Modifier.fillMaxWidth(),//.height(130.dp),
                painter = painterResource(id = R.drawable.ic_taco),
                contentDescription = "Happy Taco"
            )

            Text(
                modifier = Modifier
                    .padding(
                        top = 40.dp,
                        bottom = 0.dp,
                        start = 0.dp,
                        end = 0.dp
                    )
                    .align(Alignment.CenterHorizontally),
                text = "WELCOME",
                fontSize = 18.sp,
                fontWeight = FontWeight.Bold
            )
            Spacer(modifier = Modifier.height(10.dp))
            Text(
                modifier = Modifier.align(Alignment.CenterHorizontally),
                text = "Please log in to continue",
                fontSize = 12.sp,
                color = Color.Gray
            )
            DefaultTextField(
                modifier = Modifier.align(Alignment.CenterHorizontally),
                value = state.email,
                onValueChange = { viewModel.onEmailInput(it) },
                label = "email",
                icon = Icons.Default.Email,
                keyboardType = KeyboardType.Email,
                errorMsg = viewModel.emailErrMsg,
                validateField = {
                    viewModel.validateEmail()
                }
            )
            DefaultTextField(
                modifier = Modifier.align(Alignment.CenterHorizontally),
                value = state.password,
                onValueChange = { viewModel.onPasswordInput(it) },
                label = "password",
                icon = Icons.Default.Lock,
                hideText = true,
                errorMsg = viewModel.passwordErrMsg,
                validateField = {
                    viewModel.validatePassword()
                }
            )

            DefaultButton(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 30.dp),
                text = "LOG IN",
                onClick = {
                    viewModel.login()
                },
                enabled = viewModel.isEnabledLoginButton
            )
        }
    }
}



