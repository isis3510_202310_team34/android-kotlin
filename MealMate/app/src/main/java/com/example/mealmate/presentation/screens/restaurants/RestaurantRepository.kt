package com.example.mealmate.presentation.screens.restaurants

import android.content.ContentValues
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.mealmate.domain.model.Restaurant
import com.example.mealmate.domain.usecase.auth.AuthUseCases
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import javax.inject.Inject

class RestaurantRepository @Inject constructor(
    firestore: FirebaseFirestore,
    authUseCases: AuthUseCases
) {
    private val currentUser = authUseCases.getCurrentUser()

    private val restaurantsRef = firestore.collection("restaurants")
    private val usersRef = firestore.collection("users")
    private val db = Firebase.firestore

    fun getRestaurants(): MutableLiveData<List<Restaurant>> {
        val result = MutableLiveData<List<Restaurant>>()
        val restaurants = mutableListOf<Restaurant>()

        restaurantsRef.get().addOnSuccessListener { querySnapshot ->
            querySnapshot.forEach { document ->
                val restaurant = document.toObject(Restaurant::class.java)
                restaurant.id = document.id
                restaurants.add(restaurant)
            }

            currentUser?.let { user ->
                usersRef.document(user.uid).get().addOnSuccessListener { userDoc ->
                    val favoriteRestaurants = userDoc["favorite_restaurants"] as? List<*>
                    favoriteRestaurants?.let { favorites ->
                        restaurants.forEach { restaurant ->
                            restaurant.isFavorite = favorites.contains(restaurant.id)
                        }
                    }
                    result.value = restaurants
                }
            } ?: run {
                result.value = restaurants
            }
        }

        return result
    }

    /*fun toggleFavorite(restaurant: Restaurant) {
        currentUser?.let { user ->
            val favoriteRestaurantsRef = usersRef.document(user.uid).collection("favorite_restaurants")

            if (restaurant.isFavorite) {
                // Remove restaurant from favorites list
                favoriteRestaurantsRef.document(restaurant.id).delete()
            } else {
                // Add restaurant to favorites list
                favoriteRestaurantsRef.document(restaurant.id).set(mapOf("id" to restaurant.id))
            }
        }
    }*/

    fun updateFavorites(restaurantId: String, isFavorite: Boolean) {
        val uid = currentUser?.uid
        uid?.let {
            val userRef = db.collection("users").document(it)
            if (isFavorite) {
                userRef.update("favorite_restaurants", FieldValue.arrayUnion(restaurantId))
                    .addOnSuccessListener {
                        Log.d(ContentValues.TAG, "Added $restaurantId to favorites")
                    }
                    .addOnFailureListener { e ->
                        Log.w(ContentValues.TAG, "Error adding $restaurantId to favorites", e)
                    }
            } else {
                userRef.update("favorite_restaurants", FieldValue.arrayRemove(restaurantId))
                    .addOnSuccessListener {
                        Log.d(ContentValues.TAG, "Removed $restaurantId from favorites")
                    }
                    .addOnFailureListener { e ->
                        Log.w(ContentValues.TAG, "Error removing $restaurantId from favorites", e)
                    }
            }
        }
    }
}