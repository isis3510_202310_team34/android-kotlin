package com.example.mealmate.presentation.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)

// MealMate colors
val Black = Color(0xFFFF0A0908)
val White = Color(0xFFFFFAFAFF)
val Yellow = Color(0xFFFFFFD447)
val B_red = Color(0xFFFFF64740)
val D_red = Color(0xFFFF983628)

