package com.example.mealmate.presentation.screens.restaurants.components


import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Favorite
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.scale
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import com.example.mealmate.domain.model.Restaurant
import androidx.compose.animation.core.*
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.*
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.mealmate.presentation.screens.restaurants.RestaurantViewModel
import kotlinx.coroutines.launch

@Composable
fun RestaurantItem(restaurant: Restaurant, viewModel: RestaurantViewModel = hiltViewModel(),) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp),
        elevation = 4.dp,
        shape = RoundedCornerShape(size = 12.dp),
        backgroundColor = MaterialTheme.colors.background
    ) {
        println("Restaurante melo: " + restaurant)
        CardInfo(restaurant, viewModel)
    }
}

@Composable
fun CardInfo(
    restaurant: Restaurant,
    viewModel: RestaurantViewModel
    ) {
    Row(
        modifier = Modifier.padding(all = 12.dp),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Center,
    ) {
        AsyncImage(
            modifier = Modifier
                .clip(shape = RoundedCornerShape(10.dp))
                .size(size = 62.dp),
            model = restaurant.image,
            contentDescription = "Imágen del restaurante",
            contentScale = ContentScale.Crop
        )
        Spacer(modifier = Modifier.width(width = 8.dp)) // gap between image and text
        Text(
            color = MaterialTheme.colors.primary,
            text = restaurant.name,
            fontSize = 20.sp,
            fontWeight = FontWeight.Medium
        )
        Spacer(modifier = Modifier.width(width = 8.dp))
        Spacer(modifier = Modifier.width(width = 8.dp))
        Spacer(modifier = Modifier.width(width = 8.dp))
        HeartAnimation(restaurant, viewModel)
    }
}
@Composable
fun HeartAnimation(
    restaurant: Restaurant,
    viewModel: RestaurantViewModel
)
{
    val interactionSource = MutableInteractionSource()

    val coroutineScope = rememberCoroutineScope()

    var enabled by remember {
        mutableStateOf(false)
    }

    val scale = remember {
        Animatable(1f)
    }

    Icon(
        imageVector = Icons.Outlined.Favorite,
        contentDescription = "Like the product",
        tint = if (restaurant.isFavorite) MaterialTheme.colors.secondary else MaterialTheme.colors.primary,
        modifier = Modifier
            .scale(scale = scale.value)
            .size(size = 30.dp)
            .clickable(
                interactionSource = interactionSource,
                indication = null
            ) {
                viewModel.toggleFavorite(restaurant)
                enabled = !enabled
                coroutineScope.launch {
                    scale.animateTo(
                        0.8f,
                        animationSpec = tween(100),
                    )
                    scale.animateTo(
                        1f,
                        animationSpec = tween(100),
                    )
                }
            }
    )
}
