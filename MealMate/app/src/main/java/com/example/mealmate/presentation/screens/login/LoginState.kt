package com.example.mealmate.presentation.screens.login

data class LoginState(
    val email: String = "",
    val password: String = ""
)
