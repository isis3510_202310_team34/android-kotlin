package com.example.mealmate.presentation.screens.signup

data class SignupState(
    val name: String = "",
    val phone: String = "",
    val role: String = "",
    val email: String = "",
    val password: String = "",
    val confirmPassword: String = ""

)
