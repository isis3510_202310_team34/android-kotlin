package com.example.mealmate.presentation.screens.signup

import android.util.Patterns
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.FirebaseUser
import com.example.mealmate.domain.model.Response
import com.example.mealmate.domain.model.User
import com.example.mealmate.domain.usecase.auth.AuthUseCases
import com.example.mealmate.domain.usecase.users.UsersUseCases
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SignupViewModel @Inject constructor(private val authUseCases: AuthUseCases, private val usersUseCases: UsersUseCases): ViewModel() {

    // STATE FORM
    var state by mutableStateOf(SignupState())
        private set

    // NAME
    var isNameValid by mutableStateOf(false)
        private set
    var nameErrMsg by mutableStateOf("")
        private set

    // PHONE
    var isPhoneValid by mutableStateOf(false)
        private set
    var phoneErrMsg by mutableStateOf("")
        private set

    // ROLE
    var isRoleValid by mutableStateOf(false)
        private set
    var roleErrMsg by mutableStateOf("")
        private set

    // EMAIL
    var isEmailValid by mutableStateOf(false)
        private set
    var emailErrMsg by mutableStateOf("")
        private set

    // PASSWORD
    var isPasswordValid by mutableStateOf(false)
        private set
    var passwordErrMsg by mutableStateOf("")
        private set

    // CONFIRMAR CONTRASENA
    var isconfirmPassword by mutableStateOf(false)
        private set
    var confirmPasswordErrMsg by mutableStateOf("")
        private set

    // ENABLE BUTTON
    var isEnabledLoginButton = false

    var signupResponse by mutableStateOf<Response<FirebaseUser>?>(null)
        private set

    var user = User()

    fun onNameInput(name: String) {
        state = state.copy(name = name)
    }

    fun onPhoneInput(phone: String) {
        state = state.copy(phone = phone)
    }

    fun onRoleInput(role: String) {
        state = state.copy(role = role)
    }

    fun onEmailInput(email: String) {
        state = state.copy(email = email)
    }

    fun onPasswordInput(password: String) {
        state = state.copy(password = password)
    }

    fun onConfirmPasswordInput(confirmPassword: String) {
        state = state.copy(confirmPassword = confirmPassword)
    }

    fun onSignup() {
        user.name = state.name
        user.phone = state.phone
        user.role = state.role
        user.email = state.email
        user.password = state.password
        signup(user)
    }

    fun createUser() = viewModelScope.launch {
        user.id = authUseCases.getCurrentUser()!!.uid
        usersUseCases.create(user)
    }

    fun signup(user: User) = viewModelScope.launch {
        signupResponse = Response.Loading
        val result = authUseCases.signup(user)
        signupResponse = result
    }

    fun enabledLoginButton() {
        isEnabledLoginButton =
            isEmailValid &&
            isPasswordValid &&
            isNameValid &&
            isPhoneValid &&
            isRoleValid &&
            isconfirmPassword
    }

    fun validateConfirmPassword() {
        if (state.password == state.confirmPassword) {
            isconfirmPassword = true
            confirmPasswordErrMsg = ""
        }
        else {
            isconfirmPassword = false
            confirmPasswordErrMsg = "The passwords do not match"
        }
        enabledLoginButton()
    }

    fun validateName() {
        if (state.name.length >= 5) {
            isNameValid = true
            nameErrMsg = ""
        }
        else {
            isNameValid = false
            nameErrMsg = "It has to be at least 5 characters long"
        }

        enabledLoginButton()
    }

    fun validatePhone() {
        println(state.phone.length)
        if (state.phone.length >= 10) {
            isPhoneValid = true
            phoneErrMsg = ""
        }
        else {
            isPhoneValid = false
            phoneErrMsg = "It has to be at least 10 characters long"
        }

        enabledLoginButton()
    }

    fun validateRole() {
        if (state.role.isNotEmpty()) {
            isRoleValid = true
            roleErrMsg = ""
        }
        else {
            isRoleValid = false
            roleErrMsg = "You must choose a role in order to sign up"
        }

        enabledLoginButton()
    }

    fun validateEmail() {
        // ES UN EMAIL VALIDO
        if (Patterns.EMAIL_ADDRESS.matcher(state.email).matches()) {
            isEmailValid = true
            emailErrMsg = ""
        }
        else {
            isEmailValid = false
            emailErrMsg = "That is not a valid email"
        }

        enabledLoginButton()
    }

    fun validatePassword() {
        if (state.password.length >= 6) {
            isPasswordValid = true
            passwordErrMsg = ""
        }
        else {
            isPasswordValid = false
            passwordErrMsg = "It has to be at least 6 characters long"
        }

        enabledLoginButton()
    }


}