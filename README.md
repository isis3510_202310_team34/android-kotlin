# Android Kotlin

MVVM - Clean Architecture

En la carpeta del proyecto ir a la carpeta ui y crear una carpeta por feature (e.g. login), en ella se encontraran las siguientes carpetas:
* Domain: la capa de dominio que es donde se encuentra la logica de negocio. (use case)
* Data: Persistencia de datos, llamadas a retrofit, ... 
* UI: Donde se encuentran las views -activity, fragment- (se usa jetpack entonces las views se hacen con compose no como xml) y los view models

Intentar que los componentes no sean muy grandes, atomizar lo maximo posible
* Como se usa Jetpack Compose usar solo activities (no usar fragments)
* Manejar los cambios del screen (view) desde el viewmodel usando cosas como el live data


## Notas utiles compose
Para TextField en el cual queremos que no se agreguen mas lineas al presionar enter usar singleLine=true y maxLines=1.

Jerarquia: Cada layout puede modificar los items que contiene en primer nivel, estos van a necesitar el modificador de esos items de segundo nivel. Solo se puede modificar un escalon abajo.

Usar  _<nombre> para nombrar atributos privados
## Pasos
1. Crear el view: crear activity/fragment y screen (ui), agregar la screen al on create del activity respectivo
2. Crear el VM:
2.1. Crear los estados con live data (estados mutables de la vista) dentro del vm. 


Live Data: conexion ente activity y viewModel, cuando hay un cambio en el viewModel life data avisara atraves del patron observer al activity